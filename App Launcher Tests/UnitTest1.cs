﻿using System;
using App_Launcher.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App_Launcher;
using static App_Launcher.Query.Executing;
using App_Launcher.Query;
using System.Diagnostics;

namespace App_Launcher_Tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void StorageManager_not_creates_duplicate_Storages()
        {
            var S1 = StorageManager.Get(StorageTypes.Shared, "keck");
            var S2 = StorageManager.Get(StorageTypes.Shared, "keck");

            Assert.AreEqual(S1, S2);
        }

        [TestMethod]
        public void Actors_priority_order_is_correct()
        {
            var executed = string.Empty;
            var executors = ActorStore.GetFor<CommandExecutor>();

            Action<string> addExecuted = msg =>
            {
                Debug.WriteLine(msg);
                executed += $" {msg}";
            };

            var exer_high_1 = new CommandExecutor((query, args) =>
            {
                addExecuted("h1");
                return ExecuteResult.Skip;
            });
            var exer_high_2 = new CommandExecutor((query, args) =>
            {
                addExecuted("h2");
                return ExecuteResult.Skip;
            });
            var exer_high_3 = new CommandExecutor((query, args) =>
            {
                addExecuted("h3");
                return ExecuteResult.Skip;
            });
            var exer_default_1 = new CommandExecutor((query, args) =>
            {
                addExecuted("d1");
                return ExecuteResult.Skip;
            });
            var exer_default_2 = new CommandExecutor((query, args) =>
            {
                addExecuted("d2");
                return ExecuteResult.Skip;
            });
            var exer_default_3 = new CommandExecutor((query, args) =>
            {
                addExecuted("d3");
                return ExecuteResult.Skip;
            });
            var exer_low_1 = new CommandExecutor((query, args) =>
            {
                addExecuted("l1");
                return ExecuteResult.Skip;
            });
            var exer_low_2 = new CommandExecutor((query, args) =>
            {
                addExecuted("l2");
                return ExecuteResult.Skip;
            });
            var exer_low_3 = new CommandExecutor((query, args) =>
            {
                addExecuted("l3");
                return ExecuteResult.Skip;
            });

            executors.Add(exer_default_1, Priority.Default);
            executors.Add(exer_high_1, Priority.High);
            executors.Add(exer_low_1, Priority.Low);
            executors.Add(exer_low_2, Priority.Low);
            executors.Add(exer_high_2, Priority.High);
            executors.Add(exer_low_3, Priority.Low);
            executors.Add(exer_high_3, Priority.High);
            executors.Add(exer_default_2, Priority.Default);
            executors.Add(exer_default_3, Priority.Default);

            foreach (var exe in executors)
                exe(string.Empty, string.Empty);

            executed = executed.Trim();
            Assert.AreEqual("h3 h2 h1 d3 d2 d1 l3 l2 l1", executed);
        }
    }
}
