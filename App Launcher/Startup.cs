﻿using App_Launcher.Plugins;

namespace App_Launcher
{
    internal sealed class Startup
    {
        public Startup(AppUI ui)
        {
            PluginLoader.LoadAll(); 

            WinRHook.Intercept();
            WinRHook.OnWinRCatched += ui.Show;
        }
    }
}
