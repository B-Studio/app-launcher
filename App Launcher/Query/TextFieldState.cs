﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher.Query
{
    public delegate void TextFieldStateHandler(in string queryString, out int selectionStart, out int selectionEnd);
    public class TextFieldState
    {
        static TextFieldState()
        {
            SelectAll = new TextFieldState
            {
                Handle = (in string queryStr, out int selStart, out int selEnd) =>
                { selStart = 0; selEnd = queryStr.Length; }
            };
            CaretFirst = new TextFieldState
            {
                Handle = (in string queryStr, out int selStart, out int selEnd) =>
                { selStart = 0; selEnd = 0; }
            };
            CaretLast = new TextFieldState
            {
                Handle = (in string queryStr, out int selStart, out int selEnd) =>
                { selStart = queryStr.Length; selEnd = queryStr.Length; }
            };
        }

        public static readonly TextFieldState SelectAll;
        public static readonly TextFieldState CaretFirst;
        public static readonly TextFieldState CaretLast;
        internal static TextFieldState Default => SelectAll;

        public TextFieldStateHandler Handle { get; set; }

        public static TextFieldState Create(TextFieldStateHandler handler)
               => new TextFieldState { Handle = handler };
    }
}
