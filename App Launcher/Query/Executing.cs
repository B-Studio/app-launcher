﻿using System;
using System.Collections.Generic; 
using System.Linq; 
using System.Windows;

namespace App_Launcher.Query
{
    public static class Executing
    {
        public delegate ExecuteResult CommandExecutor(string command, string args);

        private static string _lastErrorCommand = string.Empty;
        private static readonly ActorStore<CommandExecutor> executors = ActorStore.GetFor<CommandExecutor>();
        
        /// <param name="executor">Должен возвращать true, если команда обработана и её не следует обрабатывать следующими executor'ами</param>
        public static void AddExecutor(CommandExecutor executor, Priority priority = Priority.Default) 
              => executors.Add(executor, priority);

        internal static ExecuteResult Execute(string queryString)
        {
            queryString = queryString.Trim();

            if (queryString.Length == 0
            ||  queryString == _lastErrorCommand)
            {
                _lastErrorCommand = string.Empty;
                return ExecuteResult.Default;
            }
            
            try
            {
                var lastExecuted = ExecuteResult.Default;
                var start = SplitToAppAndArgs(queryString);

                foreach (var exe in executors)
                {
                    lastExecuted = exe(command: start.Key, args: start.Value);
                    if (lastExecuted.NeedProcNext == false) break;
                }

                if (lastExecuted.NeedSaveHistory)
                {
                    Properties.Settings.Default.LastQuery = queryString;
                    Properties.Settings.Default.Save();
                }

                _lastErrorCommand = string.Empty;
                return lastExecuted;
            }
            catch (Exception ex)
            {
                _lastErrorCommand = queryString;
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return ExecuteResult.Default;
            }
        }

        #region Helpers
        private static KeyValuePair<string, string> SplitToAppAndArgs(string srcRequest)
        {
            var src = srcRequest.Trim();
            var app = src;
            var args = string.Empty;

            if (src.StartsWith("\""))
            {
                var closingQuote = src.TrimStart('"').IndexOf('"');
                if (closingQuote == -1) return new KeyValuePair<string, string>(src, string.Empty);

                var removeStart = closingQuote + 2;
                if (removeStart < src.Length)
                {
                    app = src.Remove(removeStart);
                    args = src.Remove(0, removeStart).Trim();
                }
            }
            else
            {
                var spl = src.Split(' ');
                app = spl.First();
                args = spl.Length > 1 ? string.Join(" ", spl.Skip(1).ToArray()) : string.Empty;
            }
            return new KeyValuePair<string, string>(app, args);
        }

        private static string cropExtension(string name, bool toLower = false)
        {
            var extPos = name.LastIndexOf('.');
            if (toLower) name = name.ToLower();
            return extPos > -1 ? name.Remove(extPos) : name;
        }
        #endregion
    }
}
