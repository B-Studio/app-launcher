﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher.Query
{
    public struct TransformResult
    {
        /// <summary> Не передавать команду дальше, не выполнять </summary>
        public static readonly TransformResult Succeseed
                         = new TransformResult { NeedTransformNext = false, NeedProc = false, FieldStateAfter = TextFieldState.Default };
        /// <summary> Передать команду дальше - попробовать трансформировать или выполнить </summary>
        public static readonly TransformResult Skip
                         = new TransformResult { NeedTransformNext = true, NeedProc = true, FieldStateAfter = TextFieldState.Default };

        /// <summary> Указывает, нужно ли трансформированную команду передать следующим трансформаторам (Transformers) </summary>
        public bool NeedTransformNext;
        /// <summary> Указывает, нужно ли трансформированную команду передать исполнителям (Executors) </summary>
        public bool NeedProc;
        /// <summary> Указывает, какое состояние курсора должно иметь текстовое поле после трансформации </summary>
        public TextFieldState FieldStateAfter;
    }
}
