﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher.Query
{
    public struct ExecuteResult
    {
        /// <summary> Не скрывать форму, не передавать команду дальше, не сохранять команду в память </summary>
        public static readonly ExecuteResult Default
                         = new ExecuteResult { NeedHide = false };
        /// <summary> Не скрывать форму, передать команду дальше, не сохранять команду в память </summary>
        public static readonly ExecuteResult Skip
                         = new ExecuteResult { NeedProcNext = true };

        public bool NeedHide;
        public bool NeedProcNext;
        public bool NeedSaveHistory;
        public string ChangedQueryString;
    }
}
