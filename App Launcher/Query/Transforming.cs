﻿namespace App_Launcher.Query
{
    public static class Transforming
    {
        public delegate TransformResult QueryTransformer(ref string query);

        private static readonly ActorStore<QueryTransformer> transformers = ActorStore.GetFor<QueryTransformer>();

        public static void AddTransformer(QueryTransformer transformer, Priority priority = Priority.Default)
           => transformers.Add(transformer, priority);

        internal static TransformResult Transform(ref string query)
        {
            TransformResult lastResult = TransformResult.Skip;
            foreach (var transform in transformers)
            {
                lastResult = transform(ref query);
                if (lastResult.FieldStateAfter == null)
                    lastResult.FieldStateAfter = TextFieldState.Default;
                if (lastResult.NeedTransformNext == false) break;
            }
            return lastResult;
        }
    }
}
