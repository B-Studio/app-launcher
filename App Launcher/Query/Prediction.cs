﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher.Query
{
    public class Prediction
    {
        public string[] Matched { get; set; } = new string[] { };
        public bool IsExclusive { get; set; } = false;
    }
}
