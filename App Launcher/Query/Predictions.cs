﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace App_Launcher.Query
{
    public static class Predictions
    {
        public delegate Prediction PredictionProvider(string query);

        private static readonly ActorStore<PredictionProvider> providers = ActorStore.GetFor<PredictionProvider>();
        public static void AddProvider(PredictionProvider provider, Priority priority = Priority.Default) 
            =>   providers.Add(provider, priority);


        private static int SelectedPreIndex = -1;
        private static string SelectedPre = null;

        private static readonly string[] empty = new string[] { };
        private static string[] matched = new string[] { };

        internal static string Get(string newQuery)
        {
            var qlow = newQuery.ToLower();

            matched = empty;

            var matchedList = empty.ToList();
            foreach (var provider in providers)
            {
                var predick = provider(qlow);
                if (predick != null && predick.Matched != null && predick.Matched.Length > 0)
                {
                    if (predick.IsExclusive)
                    {
                        matched = predick.Matched;
                        break;
                    }
                    matchedList.AddRange(predick.Matched);
                }
            }
            
            if (matched == empty) // если никто не захотел IsExclusive
                matched = matchedList.ToArray();
             
            if (matched.Length == 0)
            {
                SelectedPre = null;
                SelectedPreIndex = -1;
                return null;
            }

            if (!string.IsNullOrEmpty(SelectedPre)) // если вариант был выбран стрелками
                SelectedPreIndex = Array.FindIndex(matched, p => p == SelectedPre);

            if (SelectedPreIndex == -1)
                SelectedPreIndex = 0;

            SelectedPre = matched[SelectedPreIndex];
            SelectedPre = newQuery + SelectedPre.Substring(newQuery.Length);
            return SelectedPre;
        }

        internal static string GetNext()
        {
            if (matched.Length == 0) return SelectedPre;

            if (++SelectedPreIndex >= matched.Length)
                  SelectedPreIndex = 0;

            return matched[SelectedPreIndex];
        }
        internal static string GetPrevious()
        {
            if (matched.Length == 0) return SelectedPre;

            if (--SelectedPreIndex < 0)
                SelectedPreIndex = matched.Length - 1;

            return matched[SelectedPreIndex];
        }
    }
}
