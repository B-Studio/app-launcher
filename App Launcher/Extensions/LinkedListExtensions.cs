﻿using System.Collections.Generic;

namespace App_Launcher
{
    internal static class LinkedListExtensions
    {
        internal static IEnumerable<LinkedListNode<T>> GetNodes<T>(this LinkedList<T> list)
        {
            for (var n = list.First; n != null; n = n.Next)
            {
                yield return n;
            }
            yield break;
        }
    }
}
