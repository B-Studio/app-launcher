﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher
{
    internal static class AppUiExtensions
    {
        internal static void InvokeEx(this AppUI app, Action action)
            => app.Dispatcher.Invoke(action);
    }
}
