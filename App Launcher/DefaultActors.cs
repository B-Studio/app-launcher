﻿using App_Launcher.Query;
using App_Launcher.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace App_Launcher
{
    partial class App
    {
        internal static void AddPredictionsDefault ()
        {
            Predictions.AddProvider(query =>
            {
                if ("exit".StartsWith(query.ToLower()))
                    return new Prediction { Matched = new[] { "exit" } };

                return null;
            });
        }
        internal static void AddExecutorsDefault()
        {
            Executing.AddExecutor((command, args) =>
            {
                if (command != "exit") return ExecuteResult.Skip;

                new Thread(() =>
                {
                    Thread.Sleep(100);
                    App.Terminate();
                }).Start();
                return new ExecuteResult { NeedHide = true, NeedProcNext = false, NeedSaveHistory = false };
            }, Priority.Default);

            Executing.AddExecutor((command, args) => {
                Process.Start(command, args);

                return new ExecuteResult
                {
                    NeedHide = true,
                    NeedProcNext = false,
                    NeedSaveHistory = true
                };
            }, Priority.Low);

#if DEBUG
            var st = StorageManager.Get(StorageTypes.Shared, "test");

            st.Add("sanya", "keck");
            st.Add("cock", "cockc");
            st.Add("cockafd", "co54354436ckc");
            st.Add("cockfdsf", "co7 567 7 687 68 86 ckc");

            Executing.AddExecutor((command, args) =>
            {
                if (command != "set") return ExecuteResult.Skip;

                var parts = args.Split(' ');
                var key = parts[0];
                var data = parts.Length > 1 ? string.Join(" ", parts.Skip(1)) : string.Empty;
                st.Set(key, data);

                return new ExecuteResult
                {
                    NeedHide = false,
                    NeedProcNext = false,
                    NeedSaveHistory = false,
                    ChangedQueryString = $"get {key}"
                };
            }, Priority.Highest);


            Transforming.AddTransformer((ref string query) =>
            {
                if (!query.StartsWith("get ")) return TransformResult.Skip;

                var key = string.Join(" ", query.Split(' ').Skip(1)).TrimStart();
                query = $"set {key} {st.Get(key)}";
                
                var result = new TransformResult
                {
                    NeedProc = false,
                    NeedTransformNext = false,
                    FieldStateAfter = TextFieldState.Create(
                        (in string str, out int selStart, out int selEnd) =>
                        {
                            selStart = str.LastIndexOf(' ') + 1;
                            selEnd = str.Length;
                        })
                };
                return result;
            }, Priority.High);
#endif
        }
    }
}
