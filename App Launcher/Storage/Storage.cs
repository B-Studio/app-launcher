﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App_Launcher.Storage
{
    public class Storage
    {
        private const int WRITE_DELAY_MSEC = 500;

        internal static string GetFilePath(StorageTypes type, string storageName)
            => Path.Combine(App.DataPath, $"{type.ToString().ToLower()}.{storageName}.x");

        internal Storage(StorageTypes type, string storageName)
        {
            Type = type;
            Name = storageName;
            FilePath = GetFilePath(type, storageName);

            _fileStream = getFS();
            _values = readInitialData();
        }

        ~Storage()
        {
            _fileStream.Flush();
            _fileStream.Close();
        }
         
        private FileStream _fileStream;
        private IDictionary<string, string> _values;
        private volatile int _writeAfterMsec = WRITE_DELAY_MSEC;
        private Task _writerCurrent;

        public string Name { get; }
        public StorageTypes Type { get; }
        internal string FilePath { get; }

        private FileStream getFS() 
            => new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);

        private IDictionary<string, string> readInitialData()
        {
            var content = new Dictionary<string, string>();

            using (var reader = new StreamReader(_fileStream, Encoding.UTF8))
            {
                while (!reader.EndOfStream)
                {
                    string
                        value,
                        key = string.Empty,
                        buffer = string.Empty;
                    char prev = '\0';

                    var line = reader.ReadLine();
                    foreach (var chr in line)
                    {
                        if (prev == '\\')
                        {
                            buffer += chr;
                            prev = chr;
                            continue;
                        }
                        if (chr == '\\')
                        {
                            prev = chr;
                            continue;
                        }
                        if (chr == '=' && key == string.Empty)
                        {
                            key = buffer.Trim();
                            buffer = string.Empty;
                            prev = '\0';
                            continue;
                        }
                        buffer += chr;
                        prev = chr;
                    }
                    value = buffer.TrimStart();

                    if (content.ContainsKey(key))
                        content[key] = value;
                    else
                        content.Add(key, value);
                }
            }

            _fileStream = getFS();
            return content;
        }

        private void writeAllData()
        {
            _writeAfterMsec = WRITE_DELAY_MSEC; // сбрасываем количество мсек до записи в файл

            if (_writerCurrent != null) return;
            _writerCurrent = Task.Factory.StartNew(() =>
           {
               while (_writeAfterMsec-- > 0)
                   Thread.Sleep(1);

#if DEBUG
               Debug.WriteLine("StreamWriter goes here!");
#endif
               using (var _writer = new StreamWriter(_fileStream, Encoding.UTF8))
               {
                   foreach (var line in _values)
                       _writer.WriteLine($"{line.Key} = {line.Value}");

                   _writer.Flush();
               }
               _fileStream = getFS();
           });
        }

        public string Get(string key)
        {
            if (!_values.ContainsKey(key)) return null;
            return _values[key];
        }

        public void Add(string key, string data)
        {
            if (!_values.ContainsKey(key))
                _values.Add(key, data);
            else
                _values[key] = data;

            writeAllData(); 
        }

        public void Set(string key, string data)
            => Add(key, data);
    }
}
