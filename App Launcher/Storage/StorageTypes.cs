﻿namespace App_Launcher.Storage
{ 
    public enum StorageTypes : byte
    {
        Data,
        Config,
        Shared,
        Custom
    }
}
