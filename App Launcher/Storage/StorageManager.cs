﻿using App_Launcher.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_Launcher.Storage
{
    public static class StorageManager
    {
        public static Storage Get(StorageTypes storageType, string storageName)
        {
            var storagePath = Storage.GetFilePath(storageType, storageName);
            if (_storagesCache.ContainsKey(storagePath))
                return _storagesCache[storagePath];
            
            var storage = new Storage(storageType, storageName);
            _storagesCache.Add(storagePath, storage);
            return storage;
        }

        private static Dictionary<string, Storage> _storagesCache = new Dictionary<string, Storage>(); 
    }
}
