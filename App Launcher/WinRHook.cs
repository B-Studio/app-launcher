﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace App_Launcher
{
    internal static class WinRHook
    {
        public static event Action OnWinRCatched;
        public static void Intercept()
        {
            _hookID = SetHook(_proc);
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        private const int WH_KEYBOARD_LL = 13;
        private static readonly IntPtr WM_KEYDOWN = (IntPtr)0x0100;
        private static readonly IntPtr WM_KEYUP = (IntPtr)0x0101;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }
        private static void WinR_RaiseCatched()
        {
            new Thread(() =>
            {
                Thread.Sleep(10);
                OnWinRCatched.Invoke();
            }).Start();
        }
        private static void EmulateKeyDown(int key, int delayMsec = 0)
        {
            new Thread(() =>
            {
                if (delayMsec > 0) Thread.Sleep(delayMsec);
                isEmulated = true;
                keybd_event((byte)key, 0, 0, 0);
            }).Start();
        }
        private static void EmulateKeyUp(int key, int delayMsec = 0)
        {
            new Thread(() =>
            {
                if (delayMsec > 0) Thread.Sleep(delayMsec);
                isEmulated = true;
                keybd_event((byte)key, 0, 2, 0);
            }).Start();
        }

        private static readonly IntPtr intercepted = (IntPtr)1;

        private static bool isEmulated = false;
        private static bool winPressed = false;
        private static bool winRintercepted = false;

        private static readonly int rKey = 82;
        private static readonly int winKey = 91;

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode < 0) return CallNextHookEx(_hookID, nCode, wParam, lParam);
            
            int key = Marshal.ReadInt32(lParam);
            bool isKeyDown = wParam == WM_KEYDOWN;
            bool isKeyUp = wParam == WM_KEYUP;
            bool isWinKey = key == winKey;
            bool isRKey = key == rKey;

            if (!isEmulated)
            {
                if (winPressed)
                {
                    if (isWinKey && isKeyUp)
                    {
                        if (!winRintercepted)
                        {
                            EmulateKeyDown(winKey);
                            EmulateKeyUp(winKey, 10);
                        }
                        else EmulateKeyUp(winKey, 5);
                        winRintercepted = false;
                        winPressed = false;
                        return intercepted;
                    }
                    if (isKeyDown)
                    {
                        if (isRKey)
                        {
                            winRintercepted = true;
                            WinR_RaiseCatched();
                        }
                        else
                        {
                            EmulateKeyDown(winKey);
                            EmulateKeyDown(key, 10);
                            winPressed = false;
                        }
                        return intercepted;
                    }
                }
                else if (isWinKey && isKeyDown)
                {
                    winPressed = true;
                    return intercepted;
                }
            }
            isEmulated = false;

            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }
    }
}
