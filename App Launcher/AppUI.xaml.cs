﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using App_Launcher.Query;
using System.Threading.Tasks;
using System.Windows.Media;

namespace App_Launcher
{ 
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AppUI : Window
    {
        public AppUI()
        {
            InitializeComponent();
            UI = this;
        }

        #region Screen Position
        private int posx = -1;
        private int posy = -1;

        private void SetPosFromSettings()
        {
            posx = Properties.Settings.Default.PosX;
            posy = Properties.Settings.Default.PosY;
        }

        private void PosWindowOnScreen()
        {
            if (posx == -1 || posy == -1)
                CenterWindowOnScreen();
            else
            {
                this.Left = posx;
                this.Top = posy;
            }
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = SystemParameters.PrimaryScreenWidth;
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            this.Left = Math.Round((screenWidth * .5) - (this.ActualWidth * .5), 0);
            this.Top = Math.Round((screenHeight * .5) - (this.ActualHeight * .5), 0);
        }

        private void lblWindowMove_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) this.DragMove();
            else if (e.ChangedButton == MouseButton.Right) CenterWindowOnScreen();
            else return;

            posx = Convert.ToInt32(this.Left);
            posy = Convert.ToInt32(this.Top);

            Properties.Settings.Default.PosX = posx;
            Properties.Settings.Default.PosY = posy;
            Properties.Settings.Default.Save();
        }
        #endregion

        #region Load This
        private IntPtr Handle;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Handle = new WindowInteropHelper(this).Handle;
            SetPosFromSettings();
            Window_Hide();
        }
        #endregion

        #region Show This
        [DllImport("user32.dll", SetLastError = true)]
        private static extern void SwitchToThisWindow(IntPtr hWnd, bool turnOn);

        private void Window_Show()
        { 
            this.Dispatcher.Invoke(new Action(() =>
            {
                this.Opacity = 0;
                base.Show();
                txtQuery_TextChanged_Skip = true;
                txtQuery.Text = Properties.Settings.Default.LastQuery;
                txtPrediction.Text = txtQuery.Text;
                 
                this.Opacity = 1;
                PosWindowOnScreen();
                SwitchToThisWindow(this.Handle, true); 

                txtQuery.Focus();
                txtQuery.CaretIndex = txtQuery.Text.Length;
                txtQuery.SelectAll();
            }));
            OnShow?.Invoke();
        }
        #endregion

        #region Hide This
        private void Window_Hide()
        {
            this.Opacity = 0;
            Task.Factory.StartNew(() =>
            { 
                Dispatcher.Invoke(new Action(base.Hide));
            });
            OnHide?.Invoke();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Window_Hide();
            e.Cancel = true;
        }
        #endregion

        #region Kill This
        private void TerminateApp()
        {
            new Thread(() => Process.GetCurrentProcess().Kill()).Start();
        }
        #endregion

        #region Public API
        public static AppUI UI;
        public event Action OnHide;
        public event Action OnShow;

        public TextBox TextBoxQuery => txtQuery;
        public TextBox TextBoxPrediction => txtPrediction;

        public new void Hide() => Window_Hide();
        public new void Show() => Window_Show();
        #endregion

        private bool txtQuery_TextChanged_Skip = false;
        private void txtQuery_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtQuery_TextChanged_Skip)
            {
                txtQuery_TextChanged_Skip = false;
                return;
            }
            txtPrediction.Text = Predictions.Get(txtQuery.Text) ?? txtQuery.Text;
        }

        private void txtQuery_PreviewKeyUp(object sender, KeyEventArgs eventArgs)
        {
            switch (eventArgs.Key)
            {
                case Key.Escape:
                    Window_Hide();
                    break;

                case Key.Enter:
                    var query = txtPrediction.Text;
                    var transformResult = Transforming.Transform(ref query);
                    txtQuery.Text = query;
                    Task.Factory.StartNew(() =>
                    {
                        transformResult.FieldStateAfter.Handle(query, out int s, out int e);
                        this.InvokeEx(() => txtQuery.Select(s, e));

                        if (transformResult.NeedProc)
                        {
                            var exeResult = Executing.Execute(query);

                            if (exeResult.NeedHide)
                                this.InvokeEx(Window_Hide);

                            if (exeResult.ChangedQueryString != null)
                                this.InvokeEx(() => 
                                    txtQuery.Text = exeResult.ChangedQueryString
                                );
                        }
                    });
                    break;
            }
        }

        private void txtQuery_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            void caretToEnd() => Dispatcher.Invoke(new Action(() => txtQuery.CaretIndex = txtQuery.Text.Length));
            void caretToEndAsync() => Task.Factory.StartNew(caretToEnd);

            switch (e.Key)
            {
                case Key.Down:
                    txtPrediction.Text = Predictions.GetNext()
                                      ?? txtQuery.Text;
                    break;

                case Key.Up:
                    txtPrediction.Text = Predictions.GetPrevious()
                                      ?? txtQuery.Text;
                    break;

                case Key.Left:
                    if (txtQuery.CaretIndex < txtQuery.Text.Length
                    ||  txtQuery.Text == txtPrediction.Text)
                        break;


                    txtPrediction.Text = txtQuery.Text;
                    caretToEndAsync();
                    break;

                case Key.Right:
                    if (txtQuery.CaretIndex != txtQuery.Text.Length) break;

                    txtQuery_TextChanged_Skip = true;
                    txtQuery.Text = txtPrediction.Text;
                    caretToEndAsync();
                    break;
            }
        }
    }
}
