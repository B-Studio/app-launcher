﻿using App_Launcher.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace App_Launcher.Plugins
{
    internal static class PluginLoader
    {
        private static readonly string PLUGINS_DIR = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins");
        private static LinkedList<IPlugin> plugins;

        public static void LoadAll()
        {
            plugins = new LinkedList<IPlugin>();

            if (!Directory.Exists(PLUGINS_DIR))
            {
                Directory.CreateDirectory(PLUGINS_DIR);
                return;
            }

            var pluginFiles = new DirectoryInfo(PLUGINS_DIR).GetFiles()
                .Where(f => f.Name.ToLower().EndsWith(".dll"));

            var pluginErrors = new Dictionary<string, Exception>();
            foreach (var f in pluginFiles)
            {
                try
                {
                    LoadFile(f.FullName);
                }
                catch (Exception e)
                {
                   if (!pluginErrors.ContainsKey(f.FullName)) 
                        pluginErrors.Add(f.FullName, e); 
                }
            }
             
            foreach (var plugin in plugins)
            {
                var pluginType = plugin.GetType();

                if (typeof(IPredictor).IsAssignableFrom(pluginType))
                {
                    var predPlugin = (IPredictor)plugin;
                    predPlugin.Init();
                    Predictions.AddProvider(predPlugin.Get(), plugin.Priority);
                }
                else if (typeof(ITransformer).IsAssignableFrom(pluginType))
                {
                    Transforming.AddTransformer((plugin as ITransformer).Get(), plugin.Priority);
                }
                else if (typeof(IExecutor).IsAssignableFrom(pluginType))
                {
                    Executing.AddExecutor((plugin as IExecutor).Get(), plugin.Priority);
                }
            }
        }

        private static void LoadFile(string path)
            => LoadFromAssembly(Assembly.LoadFile(path));

        private static void LoadFromAssembly(Assembly pluginAssembly)
        {
            foreach (var type in pluginAssembly.GetExportedTypes())
            {
                if (!typeof(IPlugin).IsAssignableFrom(type)) continue;

                var plugin = Activator.CreateInstance(type);
                var priority = ((IPlugin)plugin).Priority;

                plugins.AddLast((IPlugin)plugin);
            }
        }
    }
}
