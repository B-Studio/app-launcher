﻿using static App_Launcher.Query.Executing;

namespace App_Launcher.Plugins
{
    public interface IExecutor : IPlugin<CommandExecutor> { }
}
 