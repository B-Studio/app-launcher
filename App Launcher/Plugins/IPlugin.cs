﻿using System;

namespace App_Launcher.Plugins
{

    public interface IPlugin
    {
        string Name { get; }
        Priority Priority { get; }
    }
    public interface IPlugin<ActorDelegate> : IPlugin where ActorDelegate : Delegate
    {
        ActorDelegate Get();
    }
}
