﻿using static App_Launcher.Query.Predictions;

namespace App_Launcher.Plugins
{
    public interface IPredictor : IPlugin<PredictionProvider>
    {
        void Init();
    }
}
