﻿using static App_Launcher.Query.Transforming;

namespace App_Launcher.Plugins
{
    public interface ITransformer : IPlugin<QueryTransformer> { }
}
