﻿using App_Launcher.Query;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;

namespace App_Launcher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        protected static void Main(string[] args)
        {
            var proc = Process.GetCurrentProcess();
            int count = Process.GetProcesses().Where(p => p.ProcessName == proc.ProcessName).Count();

            if (count < 2 + ((args.Length > 0 && args[0] == "-is-restarted") ? 1 : 0))
            {
                App.InitInternal();
                var ui = new AppUI();
                new Startup(ui);
                new App().Run(ui);
                return;
            }
             
            MessageBox.Show("Один экземпляр App Launcher уже запущен", "", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        internal static void InitInternal()
        {
            AddPredictionsDefault();
            AddExecutorsDefault();
        }

        private static string EnsureCreated(string directoryPath)
        {
            Directory.CreateDirectory(directoryPath);
            return directoryPath;
        }

        public static string StartupPath => EnsureCreated(AppDomain.CurrentDomain.BaseDirectory);
        public static string DataPath => EnsureCreated(Path.Combine(StartupPath, "data"));
        

        public static void Restart()
        {
            Process.Start(App.ResourceAssembly.Location, "-is-restarted");
            App.Terminate();
        }

        public static void Terminate()
        {
            new Thread(() =>
            {
                Thread.Sleep(100);
                Process.GetCurrentProcess().Kill();
            }).Start();
        }
    }
}
