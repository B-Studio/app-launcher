﻿namespace App_Launcher
{ 
    public enum Priority : sbyte
    {
        Bullshit = -3,
        Lowest = -2,
        Low = -1,
        Default = 0,
        High = 1,
        Highest = 2,
        Godlike = 3
    }
}
