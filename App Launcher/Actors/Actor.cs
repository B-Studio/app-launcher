﻿using System; 

namespace App_Launcher
{
    internal struct Actor<T> where T : Delegate
    {
        public Priority Priority;
        public T Action;

        public override string ToString()
        {
            return $"{typeof(T).Name} [{Priority.ToString()}]";
        }
    }
}
