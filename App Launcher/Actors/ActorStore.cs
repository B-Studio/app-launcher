﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq; 

namespace App_Launcher
{
    internal static class ActorStore
    {
        private static Dictionary<Type, object> _stores 
                 = new Dictionary<Type, object>();

        public static ActorStore<T> GetFor<T>() where T : Delegate => Get<T>();
        public static ActorStore<T> Get<T>() where T : Delegate
        {
            if (_stores.ContainsKey(typeof(T)))
                return (ActorStore<T>)_stores[typeof(T)];

            var store = new ActorStore<T>();
            _stores.Add(typeof(T), store);

            return store;
        }
    }

    internal class ActorStore<T> : IEnumerable<T> where T : Delegate
    {
        private LinkedList<Actor<T>> actors;

        internal ActorStore()
        {
            actors = new LinkedList<Actor<T>>();
        }

        /// <summary> alias for Addactor </summary> 
        public void Add(T actor, Priority priority = Priority.Default)
                 => AddActor(actor, priority);

        public void AddActor(T actor, Priority priority = Priority.Default)
        {
            var actorNew = new Actor<T> { Action = actor, Priority = priority };
            if (actors.Count == 0)
            {
                actors.AddFirst(actorNew);
                return;
            } 

            foreach (var actorNode in actors.GetNodes())
            {
                if (priority >= actorNode.Value.Priority)
                {
                    actors.AddBefore(actorNode, actorNew);
                    return;
                }
            }
            actors.AddLast(actorNew);
        }

        public IEnumerator<T> GetEnumerator() => actors.Select(x => x.Action).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => actors.Select(x => x.Action).GetEnumerator();
    }
}
