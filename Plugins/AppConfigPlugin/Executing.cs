﻿using System.Windows.Forms;
using System.Windows.Media;
using App_Launcher;
using App_Launcher.Query;
using App_Launcher.Plugins;
using static App_Launcher.Query.Executing;

namespace AppConfigPlugin
{
    public class Execute : IExecutor
    {
        public string Name => "app_config_plugin"; 

        /// <summary> AppConfigPlugin.Execute.Priority </summary>
        public Priority Priority => Priority.Default;

        private ExecuteResult execute(string command, string args)
        {
            if (!command.StartsWith("app-")) return ExecuteResult.Skip;

            var c = command.Substring(4).Trim() + args.Trim();
            switch (c)
            {
                case "font":
                    selectFont();
                    return ExecuteResult.Default;

                case "restart":
                    App.Restart();
                    return ExecuteResult.Default;

                case "exit":
                    App.Terminate();
                    return ExecuteResult.Default;
                    
            }
            return new ExecuteResult { NeedHide = true, NeedProcNext = false, NeedSaveHistory = false };
        }

        private void selectFont()
        {
            var fontDialog = new FontDialog();
            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                AppUI.UI.TextBoxQuery.FontFamily = new FontFamily(fontDialog.Font.FontFamily.Name);
                AppUI.UI.TextBoxPrediction.FontFamily = AppUI.UI.TextBoxQuery.FontFamily;
            }
        }

        public CommandExecutor Get() => execute;
    }
}
