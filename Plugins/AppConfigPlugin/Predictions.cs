﻿using System.Linq;
using App_Launcher;
using App_Launcher.Plugins;
using App_Launcher.Query;
using static App_Launcher.Query.Predictions;

namespace AppConfigPlugin
{
    public class Predictions : IPredictor
    {
        public string Name => "app_config_plugin";
        // AppConfigPlugin.Predictions.Priority
        public Priority Priority => Priority.Default;

        private string[] appConfigCommands = new[] { "app-font", "app-colors", "app-border-radius", "app-restart", "app-exit" };

        private Prediction predict(string fromQuery)
        {
            if (!fromQuery.StartsWith("app-")) return null;

            return new Prediction
            {
                Matched = appConfigCommands.Where(com => com.StartsWith(fromQuery)).ToArray(),
                IsExclusive = true
            };
        }

        public void Init() { return; }
        public PredictionProvider Get() => predict;
    }
}
