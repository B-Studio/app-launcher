﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using App_Launcher;
using App_Launcher.Plugins;
using App_Launcher.Query;
using App_Launcher.Storage;
using Microsoft.Win32;
using static App_Launcher.Query.Predictions;

namespace WinCommandsPlugin
{
    public class Predictions : IPredictor
    {
        // WinCommandsPlugin.Predictions.Priority
        public Priority Priority => Priority.Low;
        public string Name => "win_commands_plugin";

        private List<string> allPredictions = new List<string>();

        private string[] excExtensions = new string[] { ".exe", ".com", ".bat", ".lnk" };
        private string[] builtIn = new string[] { };

        private Storage storage;
    
        private Prediction predict(string query)
        {
            if (query.Length == 0)
                 return null;
            else return new Prediction { Matched = allPredictions.Where(p => p.StartsWith(query)).ToArray() };
        }

        private string[] loadBuiltIn()
        {
            var f = new FileInfo(Path.Combine(App.StartupPath,  this.Name, ""));
            return new string[] { };
        }


        public PredictionProvider Get() => predict;

        public void Init()
        { 
            string cropExtension(string name)
            {
                var extPos = name.LastIndexOf('.');
                name = name.ToLower();
                return extPos > -1 ? name.Remove(extPos) : name;
            }
            bool isExecutable(FileInfo f)
            {
                var fname = f.Name.ToLower();
                return excExtensions.Any(e => fname.EndsWith(e));
            }

            storage = StorageManager.Get(StorageTypes.Data, "wincommands");
            
            
            var subkeys = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths").GetSubKeyNames();
            allPredictions = subkeys.Select(cropExtension).ToList();

            var windowsDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Windows));

            allPredictions.AddRange(
                windowsDir.GetFiles()
                            .Where(isExecutable)
                            .Select(f => cropExtension(f.Name))
                                .ToArray()
                );
            allPredictions.AddRange(builtIn);
            allPredictions = allPredictions.Distinct().ToList();
        }
    }
}
